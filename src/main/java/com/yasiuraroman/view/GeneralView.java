package com.yasiuraroman.view;

import com.yasiuraroman.controller.LongestPlateauController;
import com.yasiuraroman.controller.LongestPlateauControllerImp;
import com.yasiuraroman.controller.MinesweeperController;
import com.yasiuraroman.controller.MinesweeperControllerImp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class GeneralView {
    private LongestPlateauController lpController;
    private MinesweeperController minesController;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger();

    public GeneralView() {
        lpController = new LongestPlateauControllerImp();
        minesController = new MinesweeperControllerImp();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - find longest plateau");
        menu.put("2", "  2 - get sequence length");
        menu.put("3", "  3 - get start index");
        menu.put("4", "  4 - get plateau value");
        menu.put("5", "  5 - Minesweeper create field");
        menu.put("6", "  6 - Minesweeper without values");
        menu.put("7", "  7 - Minesweeper with values");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", () -> {
            Integer[] array = new Integer[]{2,3,4,4,4,3,1,3};
            lpController.findLongestPlateau(array);
        });
        methodsMenu.put("2", () -> {
            try {
                logger.info(lpController.getSequenceLength());
            } catch (NullPointerException e){
                logger.error("lpController.getSequenceLength() " + e);
            }
        });
        methodsMenu.put("3", () -> {
            try {
                logger.info(lpController.getStartIndex());
            } catch (NullPointerException e){
                logger.error("lpController.getStartIndex() " + e);
            }
        });
        methodsMenu.put("4", () -> {
            try {
                logger.info(lpController.getValue());
            } catch (NullPointerException e){
                logger.error("lpController.getValue() " + e);
            }
        });
        methodsMenu.put("5", () -> {
            minesController.createField(10,12,22);
        });
        methodsMenu.put("6", () -> {
            try {
                for (String s:minesController.printWithoutValues()
                ) {
                    logger.info(s);
                }
            } catch (NullPointerException e){
                logger.error("minesController.printWithoutValues() " + e);
            }
        });
        methodsMenu.put("7", () -> {
            try {
                for (String s:minesController.printWithValues()
                     ) {
                logger.info(s);
                }
            } catch (NullPointerException e){
                logger.error("minesController.printWithValues() " + e);
            }
        });
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
