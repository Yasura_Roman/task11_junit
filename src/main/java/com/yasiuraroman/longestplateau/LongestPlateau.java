package com.yasiuraroman.longestplateau;

public class LongestPlateau {

    private int sequenceLength;
    private int startIndex;
    private int value;

    public void lp(Integer[] array){

        startIndex = 0;
        value = array[0];
        sequenceLength = 1;

        for (int i = 1; i < array.length ; i++) {
            if(array[i] == value  && value == array[i - 1]){
                sequenceLength++;
            } else if (value < array[i]){
                value = array[i];
                sequenceLength = 1;
                startIndex = i;
            }
        }
    }

    public int getSequenceLength() {
        return sequenceLength;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getValue() {
        return value;
    }
}
