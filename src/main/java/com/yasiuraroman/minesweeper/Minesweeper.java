package com.yasiuraroman.minesweeper;

import java.util.Arrays;
import java.util.Random;

public class Minesweeper {

    public static final String BOMB = "*";
    public static final String FREE = ".";

    private static final Random random = new Random();
    private static final String[] VALUES = new String[]{".","1","2","3","4","5","6","7","8"};

    private int M;
    private int N;
    private int probability;
    private String[][] field;

    public Minesweeper(int m, int n, int probability) {
        M = m;
        N = n;
        this.probability = probability;
        createField();
    }

    private void createField(){

        if (probability < 10){
            probability = 10;
        }
        if (probability > 80){
            probability = 80;
        }
        field = new String[M][N];
        for (int i = 0; i < M; i++) {
            Arrays.fill(field[i],FREE);
        }
        int bombCount = (int) (M * N * (probability/100.0));
        for (int i = 0; i < bombCount; i++) {
            placeBomb();
        }
        AllCellValue();
    }

    private void placeBomb(){

        int col = random.nextInt(M);
        int row = random.nextInt(N);

        while ( field[col][row].equals(BOMB) ){
            col = random.nextInt(M);
            row = random.nextInt(N);
        }
        field[col][row] = BOMB;
    }

    private void AllCellValue(){
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                cellValue(i,j);
            }
        }
    }

    private void cellValue(int col, int row){

        if( field[col][row] == BOMB){
            return;
        }
        int bombCount = 0;
        for (int i = col - 1; i <=  col + 1; i++) {
            for (int j = row - 1; j <= row + 1; j++) {
                if( isValidCell(i,j) && field[i][j] == BOMB){
                    bombCount++;
                }
            }
        }
        field[col][row] = VALUES[bombCount];
    }

    private boolean isValidCell(int col, int row){
        return col >= 0 && col < M && row >= 0 && row < N ;
    }

    public static Random getRandom() {
        return random;
    }

    public static String[] getVALUES() {
        return VALUES;
    }

    public int getProbability() {
        return probability;
    }

    public String[][] getField() {
        return field;
    }
}
