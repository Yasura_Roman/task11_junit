package com.yasiuraroman.controller;

public interface LongestPlateauController {
    void findLongestPlateau(Integer[] array);
    int getSequenceLength();
    int getStartIndex();
    int getValue();
}
