package com.yasiuraroman.controller;

public interface MinesweeperController {
    void createField(int m, int n, int probability);
    String[] printWithoutValues();
    String[] printWithValues();
}
