package com.yasiuraroman.controller;

import com.yasiuraroman.minesweeper.Minesweeper;

public class MinesweeperControllerImp implements MinesweeperController {

    private StringBuilder builder;
    Minesweeper model;

    public MinesweeperControllerImp() {
    }

    @Override
    public String[] printWithoutValues() {
        String[] array = new String[model.getField().length];
        for (int i = 0; i < model.getField().length; i++) {
            builder = new StringBuilder();
            for (int j = 0; j < model.getField()[0].length; j++) {
                if (model.getField()[i][j] == Minesweeper.BOMB){
                    builder.append(" " + model.getField()[i][j]);
                } else {
                    builder.append(" " + Minesweeper.FREE);
                }
            }
            array[i] = builder.toString();
        }
        return array;
    }

    @Override
    public String[]  printWithValues(){
        String[] array = new String[model.getField().length];
        for (int i = 0; i < model.getField().length; i++) {
            builder = new StringBuilder();
            for (int j = 0; j < model.getField()[0].length; j++) {
                builder.append(" " + model.getField()[i][j]);
            }
            array[i] = builder.toString();
        }
        return array;
    }

    @Override
    public void createField(int m, int n, int probability) {
        this.model = new Minesweeper( m, n, probability);
    }
}
