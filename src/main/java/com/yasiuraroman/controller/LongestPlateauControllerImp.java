package com.yasiuraroman.controller;

import com.yasiuraroman.longestplateau.LongestPlateau;

public class LongestPlateauControllerImp implements LongestPlateauController {

    LongestPlateau model;

    public LongestPlateauControllerImp() {
        model = new LongestPlateau();
    }

    @Override
    public void findLongestPlateau(Integer[] array) {
        model.lp(array);
    }

    @Override
    public int getSequenceLength() {
        return model.getSequenceLength();
    }

    @Override
    public int getStartIndex() {
        return model.getStartIndex();
    }

    @Override
    public int getValue() {
        return model.getValue();
    }
}
