package com.yasiuraroman.minesweeper;

import com.yasiuraroman.controller.MinesweeperController;
import com.yasiuraroman.controller.MinesweeperControllerImp;
import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class MinesweeperTests {

    Logger logger = LogManager.getLogger();

    @Mock
    Minesweeper model;

    @InjectMocks
    MinesweeperController controller = new MinesweeperControllerImp();

    @BeforeEach
    public void init(){
        logger.info("Before Each Test");
        initMocks(this);
        when(model.getField()).thenReturn(new String[][]{   {"*","2"},  {"3","*"},  {"2","*"}   });
    }

    @RepeatedTest(3)
    public void testMinesweeperModel1(){
        String[] array = controller.printWithoutValues();
        TestCase.assertEquals(array[0], (" * ."));
        TestCase.assertEquals(array[1], (" . *"));
        TestCase.assertEquals(array[2], (" . *"));
    }

    @Test
    public void testMinesweeperModel2(){
        String[] array = controller.printWithValues();
        TestCase.assertEquals(array[0], (" * 2"));
        TestCase.assertEquals(array[1], (" 3 *"));
        TestCase.assertEquals(array[2], (" 2 *"));
    }

    @AfterEach
    public void afterEachTest() {
        logger.info("After Each Test");
        logger.info("=====================");
    }
}
