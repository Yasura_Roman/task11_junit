package com.yasiuraroman.longestplateau;

import com.yasiuraroman.controller.LongestPlateauController;
import com.yasiuraroman.controller.LongestPlateauControllerImp;
import junit.framework.TestCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class LongestPlateauTests {

    final static Logger logger = LogManager.getLogger();

    @Mock
    LongestPlateau model;

    @InjectMocks
    LongestPlateauController controller = new LongestPlateauControllerImp();

    @BeforeEach
    public void init(){
        logger.info("Before Each Test");
        initMocks(this);
    }

    @Test
    public void testLp1(){
        Integer[] array = new Integer[]{2,3,4,4,4,3,1,3};
        when(model.getSequenceLength()).thenReturn(3);
        when(model.getStartIndex()).thenReturn(2);
        when(model.getValue()).thenReturn(4);
        TestCase.assertTrue(controller.getSequenceLength() == 3 && controller.getStartIndex() == 2 && controller.getValue() == 4);
    }

    @Test
    public void testLp2(){
        LongestPlateau longestPlateau = new LongestPlateau();
        Integer[] array = new Integer[]{11,2,6,3,12,12,3,10,3,4,2};
        longestPlateau.lp(array);
        TestCase.assertTrue(longestPlateau.getSequenceLength() == 2 && longestPlateau.getStartIndex() == 4 && longestPlateau.getValue() == 12);
    }

    @Test
    public void testLp3(){
        Integer[] array = new Integer[]{14,14,14,13,12,15,15,2,3,14,12};
        controller.findLongestPlateau(array);
        when(model.getSequenceLength()).thenReturn(2);
        when(model.getStartIndex()).thenReturn(5);
        when(model.getValue()).thenReturn(15);
        TestCase.assertTrue(controller.getSequenceLength() == 2 && controller.getStartIndex() == 5 && controller.getValue() == 15);
    }

    @AfterEach
    public void afterEachTest() {
        logger.info("After Each Test");
        logger.info("=====================");
    }
}
